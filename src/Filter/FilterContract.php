<?php

namespace Vitrin\Infrastructure\Contracts\Filter;

/**
 * Contract for implementing data filters.
 * 
 * Provides a standardized approach for filtering data across the application.
 * It's used by repositores and applied on eloqent models
 * 
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface FilterContract
{
    //
}