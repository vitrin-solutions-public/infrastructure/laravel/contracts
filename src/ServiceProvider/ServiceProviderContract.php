<?php

namespace Vitrin\Infrastructure\Contracts\ServiceProvider;

/**
 * Contract for defining service providers.
 *
 * This interface outlines the structure for service providers in the application,
 * aiming to ensure a consistent approach to registering services, bindings,
 * and configurations within the application's service container.
 */
interface ServiceProviderContract
{
    //
}
