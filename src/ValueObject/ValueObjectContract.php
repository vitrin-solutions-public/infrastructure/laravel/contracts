<?php

namespace Vitrin\Infrastructure\Contracts\ValueObject;

/**
 * Contract for value objects.
 *
 * Marks value objects within the application, setting a standard for 
 * immutable objects that describe aspects of the domain without identity. 
 * These objects encapsulate domain data and logic.
 */
interface ValueObjectContract
{
    /**
     * Converts the value object to an array.
     *
     * This method facilitates the transformation of the value object's
     * properties into an array format, useful for serialization and 
     * integration with other data structures or interfaces.
     *
     * @return array Representation of the value object as an array.
     */
    public function toArray(): array;
}
