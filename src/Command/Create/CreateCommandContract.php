<?php

namespace Vitrin\Infrastructure\Contracts\Command\Create;

use Vitrin\Infrastructure\Contracts\Entity\EntityContract;

/**
 * Contract for create commands.
 *
 * Defines the essential responsibilities of create commands, including
 * transforming command data into an entity and authorizing the operation.
 *
 * @author Amir Khadangi <amirkhadangi920@gmai.com>
 */
interface CreateCommandContract
{
    /**
     * Transforms the command into an array.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Checks if the operation is authorized.
     *
     * @return bool True if authorized, otherwise false.
     */
    public static function authorize(): bool;
}
