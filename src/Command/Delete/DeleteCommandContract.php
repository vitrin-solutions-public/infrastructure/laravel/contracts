<?php

namespace Vitrin\Infrastructure\Contracts\Command\Delete;

/**
 * Contract for delete commands.
 *
 * Defines the necessary operations for handling delete commands,
 * including retrieving the entity identifier and performing authorization checks.
 *
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface DeleteCommandContract
{
    /**
     * Retrieves the unique identifier of the entity to be deleted.
     *
     * @return mixed The entity's unique identifier.
     */
    public function getIdentifier(): mixed;

    /**
     * Determines if the current operation is authorized.
     *
     * @return bool True if the operation is authorized, false otherwise.
     */
    public static function authorize(): bool;
}
