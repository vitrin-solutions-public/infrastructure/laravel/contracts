<?php

namespace Vitrin\Infrastructure\Contracts\Command\Update;

use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Types\ID;

/**
 * Defines the contract for update commands.
 *
 * Specifies the requirements for handling update operations within the system,
 * including identifier retrieval, conversion to entity format, and authorization checks.
 *
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface UpdateCommandContract
{
    /**
     * Gets the ID of the entity to update.
     *
     * @return string|int Entity identifier.
     */
    public function getIdentifier(): string|int;

    /**
     * Verifies authorization for the update operation.
     *
     * @return bool Authorization status.
     */
    public static function authorize(): bool;
}
