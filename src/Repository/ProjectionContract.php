<?php

namespace Vitrin\Infrastructure\Contracts\Repository;

/**
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface ProjectionContract
{
    public function writeable(): static;

    public function unwriteable(): static;

    public function isWritable(): bool;
}
