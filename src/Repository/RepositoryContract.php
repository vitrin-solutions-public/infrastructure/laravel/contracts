<?php

namespace Vitrin\Infrastructure\Contracts\Repository;

use Illuminate\Pagination\LengthAwarePaginator;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\LaravelData\DataCollection;
use Vitrin\Infrastructure\Contracts\Command\Create\CreateCommandContract;
use Vitrin\Infrastructure\Contracts\Command\Update\UpdateCommandContract;
use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Contracts\Query\Find\FindQueryContract;
use Vitrin\Infrastructure\Contracts\Query\List\ListQueryContract;

/**
 * Interface for repository operations.
 *
 * Specifies common CRUD operations and additional utilities
 * for interacting with entity data storage.
 *
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface RepositoryContract
{
    /**
     * Finds a single entity by its query criteria.
     *
     * @param FindQueryContract $query
     * @return EntityContract|null
     */
    public function find(FindQueryContract $query): ?EntityContract;

    /**
     * Finds a single entity by its query criteria.
     * If not found, throws a not found exception.
     *
     * @throws NotFoundExceptionInterface
     * @param FindQueryContract $query
     * @return EntityContract
     */
    public function findOrFail(FindQueryContract $query): EntityContract;

    /**
     * Retrieves entities in a paginated format.
     *
     * @param ListQueryContract $query
     * @return LengthAwarePaginator
     */
    public function paginated(ListQueryContract $query): LengthAwarePaginator;

    /**
     * Finds multiple entities by their IDs.
     *
     * @param ListQueryContract $query
     * @return DataCollection
     */
    public function list(ListQueryContract $query): DataCollection;

    /**
     * Clones a model and updates it with new data.
     */
    public function clone($model, array $data);

    /**
     * Persists a new entity to storage.
     *
     * @param CreateCommandContract $command
     * @return EntityContract
     */
    public function create(CreateCommandContract $command): EntityContract;

    /**
     * Creates multiple new entities from provided data.
     */
    public function createMany(array $data);

    /**
     * Updates an existing entity in storage.
     *
     * @param UpdateCommandContract $command
     * @return EntityContract
     */
    public function update(UpdateCommandContract $command): EntityContract;

    /**
     * Increments a specified column's value.
     */
    public function increment($model, string $column, float $amount = 1);

    /**
     * Decrements a specified column's value.
     */
    public function decrement($model, string $column, float $amount = 1);

    /**
     * Incrementally updates multiple columns.
     */
    public function incrementEach($model, array $values);

    /**
     * Decrementally updates multiple columns.
     */
    public function decrementEach($model, array $values);

    /**
     * Updates multiple entities with the same data.
     */
    public function updateMany(array $ids, array $data): int;

    /**
     * Deletes a single entity by ID.
     */
    public function delete(int $id): bool;

    /**
     * Deletes multiple entities by their IDs.
     */
    public function deleteMany(array $ids): int;
}
