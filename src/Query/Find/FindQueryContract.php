<?php

namespace Vitrin\Infrastructure\Contracts\Query\Find;

/**
 * Contract for find queries.
 *
 * Defines the essential operations for find query implementations, including
 * retrieving an entity's identifier and performing authorization checks.
 *
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface FindQueryContract
{
    /**
     * Retrieves the entity's unique identifier for the query.
     *
     * @return int|string The identifier of the entity to find.
     */
    public function getIdentifier(): int|string;

    /**
     * Determines if the query operation is authorized.
     *
     * @return bool True if authorized, false otherwise.
     */
    public static function authorize(): bool;
}
