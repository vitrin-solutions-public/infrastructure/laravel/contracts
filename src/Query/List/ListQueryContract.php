<?php

namespace Vitrin\Infrastructure\Contracts\Query\List;

/**
 * Contract for list queries.
 *
 * Outlines methods for handling list queries, including conversion to array,
 * pagination details, and authorization checks.
 * 
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface ListQueryContract
{
    /**
     * Converts query parameters to an array.
     *
     * @return array Query parameters.
     */
    public function toArray(): array;

    /**
     * Gets the current page number for pagination.
     *
     * @return int Current page number.
     */
    public function getPage(): int;

    /**
     * Gets the number of items per page for pagination.
     *
     * @return int Number of items per page.
     */
    public function getPerPage(): int;

    /**
     * Checks if the list query is authorized.
     *
     * @return bool True if authorized, otherwise false.
     */
    public static function authorize(): bool;
}