<?php

namespace Vitrin\Infrastructure\Contracts\Entity;

use Vitrin\Infrastructure\Contracts\Repository\RepositoryContract;
use Vitrin\Infrastructure\Types\ID;

/**
 * Core contract for entities within the application.
 *
 * Defines essential functionalities that all entities must implement, 
 * including methods for identifier retrieval and conversion to array format.
 * Commented-out methods hint at potential extended interactions with data storage,
 * such as refresh, store, delete, and clone operations.
 *
 * @template T The type of entity being represented.
 */
interface EntityContract
{
    /**
     * Retrieves the unique identifier of the entity.
     *
     * @return ID The entity's unique identifier.
     */
    public function getIdentifier(): ID;
    
    /**
     * Converts the entity to an array format.
     *
     * Useful for serialization and interaction with data storage layers.
     *
     * @return array The entity represented as an array.
     */
    public function toArray(): array;

    // /**
    //  * update changes of entity to data storage and sync model
    //  *
    //  * @return T
    //  */
    // public function refresh(): EntityContract;

    // /**
    //  * store the entity to data storage from repository layer
    //  *
    //  * @return T
    //  */
    // public function store(): EntityContract;

    // /**
    //  * delete entity from data storage
    //  *
    //  * @return boolean
    //  */
    // public function delete(): bool;
    
    // /**
    //  * clone a model and fill it's attribiutes with the given data array
    //  *
    //  * @param array $data
    //  * @return T
    //  */
    // public function clone(array $data = []): EntityContract;
}