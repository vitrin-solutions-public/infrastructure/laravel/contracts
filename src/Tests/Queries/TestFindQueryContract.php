<?php

namespace Vitrin\Infrastructure\Contracts\Tests\Queries;

/**
 * Contract for testing find query functionalities.
 *
 * Defines the required tests to ensure proper authorization and functionality
 * of find queries across different user permissions and use cases.
 */
interface TestFindQueryContract
{
    /**
     * Verify find query access restrictions for guest users.
     */
    public function test_for_guest(): void;

    /**
     * Ensure unauthorized users cannot execute find queries.
     */
    public function test_for_unauthorized_user(): void;

    /**
     * Confirm allowed users can successfully execute find queries.
     */
    public function test_for_allowed_user(): void;

    /**
     * Test the successful retrieval and display of a new item.
     */
    public function test_show_new_item(): void;
}
