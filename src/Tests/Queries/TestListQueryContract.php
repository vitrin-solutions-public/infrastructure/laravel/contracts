<?php

namespace Vitrin\Infrastructure\Contracts\Tests\Queries;

/**
 * Contract for testing list query functionalities.
 *
 * Specifies test methods to ensure list queries behave correctly under various
 * user authorization scenarios, including guest, unauthorized, and allowed users,
 * as well as verifying the correct output when a new item is listed.
 */
interface TestListQueryContract
{
    /**
     * Verifies that guest users cannot access list queries.
     */
    public function test_for_guest(): void;

    /**
     * Ensures unauthorized users are prevented from accessing list queries.
     */
    public function test_for_unauthorized_user(): void;

    /**
     * Confirms that authorized users can successfully execute list queries.
     */
    public function test_for_allowed_user(): void;

    /**
     * Tests the list query's ability to correctly display new items.
     */
    public function test_show_new_item(): void;
}
