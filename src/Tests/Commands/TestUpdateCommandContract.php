<?php

namespace Vitrin\Infrastructure\Contracts\Tests\Commands;

/**
 * Contract for testing update command behavior.
 *
 * Specifies test cases that should be implemented to verify the behavior of update
 * commands across different scenarios, including permission checks and data validation.
 */
interface TestUpdateCommandContract
{
    /**
     * Ensures guest users cannot perform update operations.
     */
    public function test_for_guest(): void;

    /**
     * Verifies unauthorized users are prevented from updating.
     */
    public function test_for_unauthorized_user(): void;

    /**
     * Confirms allowed users can successfully initiate updates.
     */
    public function test_for_allowed_user(): void;

    /**
     * Checks handling of updates with invalid data inputs.
     */
    public function test_for_invalid_data(): void;

    /**
     * Validates the correct item is updated and response structure.
     */
    public function test_show_new_item(): void;
}
