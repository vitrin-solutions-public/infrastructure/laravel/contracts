<?php

namespace Vitrin\Infrastructure\Contracts\Tests\Commands;

/**
 * Contract for delete command tests.
 *
 * Outlines the structure for testing delete command functionalities, ensuring
 * comprehensive coverage across different user permissions and scenarios.
 */
interface TestDeleteCommandContract
{
    /**
     * Tests behavior for guest users attempting to delete.
     */
    public function test_for_guest(): void;

    /**
     * Tests behavior for unauthorized users attempting to delete.
     */
    public function test_for_unauthorized_user(): void;

    /**
     * Tests behavior for authorized users performing a delete operation.
     */
    public function test_for_allowed_user(): void;
}
