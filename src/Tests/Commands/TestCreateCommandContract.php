<?php

namespace Vitrin\Infrastructure\Contracts\Tests\Commands;

/**
 * Contract for testing create command functionalities.
 *
 * Outlines essential test cases to ensure the create command operates correctly
 * under various scenarios, including authentication and validation checks.
 * 
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface TestCreateCommandContract
{
    /**
     * Tests the command's response to guest access.
     */
    public function test_for_guest(): void;

    /**
     * Verifies the command's handling of unauthorized users.
     */
    public function test_for_unauthorized_user(): void;

    /**
     * Ensures the command functions correctly for users with the appropriate permissions.
     */
    public function test_for_allowed_user(): void;

    /**
     * Checks the command's validation logic for handling invalid input data.
     */
    public function test_for_invalid_data(): void;

    /**
     * Validates the successful creation and correct data representation of a new item.
     */
    public function test_show_new_item(): void;
}
