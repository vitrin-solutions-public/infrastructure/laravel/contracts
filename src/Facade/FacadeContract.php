<?php

namespace Vitrin\Infrastructure\Contracts\Facade;

/**
 * Defines the base contract for facades within the application.
 *
 * This interface serves as a marker for facade classes, allowing them
 * to be easily identified and managed within the infrastructure layer. 
 * It facilitates a consistent approach to implementing facade patterns,
 * ensuring that underlying class functionalities can be statically accessed
 * through facade classes.
 * 
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface FacadeContract
{
    //
}