<?php

namespace Vitrin\Infrastructure\Contracts\Model;

/**
 * Interface for models to include dynamic query filtering.
 * 
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
interface ModelContract
{
    /**
     * Applies filters to a query based on input.
     * 
     * @param mixed $query The query builder instance.
     * @param array $input Filtering criteria.
     * @param string|null $filter Specific filter class name.
     * @return mixed Modified query builder.
     */
    public function scopeFilter($query, array $input = [], $filter = null);
}