<?php

namespace Vitrin\Infrastructure\Contracts\Service;

use Vitrin\Infrastructure\Contracts\Command\Create\CreateCommandContract;
use Vitrin\Infrastructure\Contracts\Command\Delete\DeleteCommandContract;
use Vitrin\Infrastructure\Contracts\Command\Update\UpdateCommandContract;
use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Contracts\Query\Find\FindQueryContract;
use Vitrin\Infrastructure\Contracts\Query\List\ListQueryContract;
use Vitrin\Infrastructure\Types\ID;

/**
 * @template T
 */
interface CrudServiceContract
{
    // /**
    //  * return the paginated result base on the given criteria
    //  *
    //  * @param ListQueryContract $query
    //  * @return \Illuminate\Pagination\LengthAwarePaginator
    //  */
    // public function list(ListQueryContract $query);

    // /**
    //  * find a model by it's identifier
    //  *
    //  * @param FindQueryContract $query
    //  * @return T
    //  */
    // public function find($query): EntityContract;

    // /**
    //  * create a new model and save it in storage base on the given data
    //  *
    //  * @param CreateCommandContract $command
    //  * @return T
    //  */
    // public function create(CreateCommandContract $command): EntityContract;

    // /**
    //  * find a model by it's identifier and update it base on the given data
    //  *
    //  * @param UpdateCommandContract $command
    //  * @return T
    //  */
    // public function update(UpdateCommandContract $command): EntityContract;

    // /**
    //  * find a model by it's identifier and delete it
    //  *
    //  * @param DeleteCommandContract $command
    //  * @return bool
    //  */
    // public function delete(DeleteCommandContract $command): bool;
}