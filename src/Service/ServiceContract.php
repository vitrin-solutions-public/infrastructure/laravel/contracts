<?php

namespace Vitrin\Infrastructure\Contracts\Service;

/**
 * Base contract for service layer classes.
 *
 * Defines a standard structure for services within the application,
 * ensuring consistency and interoperability across the service layer.
 * Services implementing this contract should provide business logic
 * and interactions between the domain and infrastructure layers.
 */
interface ServiceContract
{
    //
}
