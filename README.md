# 🧑‍⚖️ Vitrin Solutions Contract
This is a **so simple** but **so important** packge that inclues a lot of interface files as our contracts.🔥

![alt text](/assets/cover.webp)

## 🗒️ Description
This package provides a comprehensive set of interfaces (contracts) that define the core functionalities and standards for various components within the application. It serves as the foundation for ensuring a consistent implementation of domain entities, services, repositories, value objects, and test cases accross all the projects.

## 🔍 Overview
The contracts package includes interfaces for:
- Commands and Queries: Define standard operations for CQRS patterns.
- Entities: Standardize core entity behaviors.
- Repositories: Outline data access and manipulation methods.
- Services: Define business logic and service layer interactions.
- Value Objects: Ensure immutability and domain-specific data integrity.
- Test Contracts: Provide a structure for unit and feature tests.

## 🏁 Installation
To install the Generator, first of all, you need to define this repository in you **composer.json** file

```json
// composer.json
...
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/vitrin-solutions/infrastructure/laravel/contracts.git"
    }
],
"require": {
    //
}
...
```

then, run the following command in your Laravel project:

```bash
composer require vitrin-infrastructure/contracts
``` 

## 🤟 Usage
Implement these contracts in your application to enforce consistency and adherence to domain-driven design principles. For example, to create a new entity:

```php
namespace App\Domain\Entities;

use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Types\ID;

class UserEntity implements EntityContract {
    // Implementation details...
}

```

## 📚 Documentation
This is the list of all the available contracts:

| Contract                                                                         | Description                                                                                   |
|----------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| `Vitrin\Infrastructure\Contracts\Command\Create\CreateCommandContract`          | Defines operations for creating entities, including authorization checks.                     |
| `Vitrin\Infrastructure\Contracts\Command\Delete\DeleteCommandContract`          | Outlines deletion operations for entities, including getting identifiers and authorization.   |
| `Vitrin\Infrastructure\Contracts\Command\Update\UpdateCommandContract`          | Specifies update operations, including data transformation to entities and authorization.     |
| `Vitrin\Infrastructure\Contracts\Entity\EntityContract`                         | Defines essential entity behaviors, like identifier retrieval and array conversion.           |
| `Vitrin\Infrastructure\Contracts\Facade\FacadeContract`                         | Marker interface for facade classes, facilitating static access to underlying logic.          |
| `Vitrin\Infrastructure\Contracts\Filter\FilterContract`                         | Standardizes data filtering across applications for dynamic query modifications.              |
| `Vitrin\Infrastructure\Contracts\Model\ModelContract`                           | Enhances model functionality with methods like dynamic query filtering.                       |
| `Vitrin\Infrastructure\Contracts\Query\Find\FindQueryContract`                  | Defines the contract for find queries, including identifier retrieval and authorization.      |
| `Vitrin\Infrastructure\Contracts\Query\List\ListQueryContract`                  | Outlines the structure for list queries, including pagination details and authorization.      |
| `Vitrin\Infrastructure\Contracts\Repository\RepositoryContract`                 | Specifies common CRUD operations and utilities for entity data storage.                       |
| `Vitrin\Infrastructure\Contracts\Service\ServiceContract`                       | Base contract for service layer classes, defining business logic and service interactions.    |
| `Vitrin\Infrastructure\Contracts\ServiceProvider\ServiceProviderContract`       | Contract for defining service providers, standardizing service registration.                  |
| `Vitrin\Infrastructure\Contracts\Tests\Commands\TestCreateCommandContract`      | Contract for testing create command functionalities across different user permissions.        |
| `Vitrin\Infrastructure\Contracts\Tests\Commands\TestDeleteCommandContract`      | Defines the testing structure for delete command functionalities.                             |
| `Vitrin\Infrastructure\Contracts\Tests\Commands\TestUpdateCommandContract`      | Contract for testing update command operations, including invalid data handling.              |
| `Vitrin\Infrastructure\Contracts\Tests\Queries\TestFindQueryContract`           | Outlines testing strategies for find query operations, including authorization checks.        |
| `Vitrin\Infrastructure\Contracts\Tests\Queries\TestListQueryContract`           | Defines the contract for testing list query functionalities and data integrity.               |
| `Vitrin\Infrastructure\Contracts\ValueObject\ValueObjectContract`               | Marker interface for value objects, ensuring immutability and domain-specific data integrity. |


> ## 🪄 Go to next level
> Creating all the files and implment the correct contract is a really boring and time consuming task, instead of that, you can use **Vitrin Solutions Generator** to generate all the files what you need automatically 🔥.\
> [Link to generator documentation](https://gitlab.com/vitrin-solutions/infrastructure/laravel/generator)


## 👨‍💻👩‍💻 Contributing
Contributions to the Contracts Package are welcome! Please ensure your pull requests are well-documented and follow the existing coding standards.

## 👨‍💻 Author
This package is under active development by Amir Khadangi.

## 🪪 License
This project is licensed under the MIT License. See the LICENSE file for details.